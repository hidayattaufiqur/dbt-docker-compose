const express = require("express");
const redis = require("redis");

const app = express();

const client = redis.createClient({
    host: "redis-server",
    port: 6379,
});

client.set("counter", 0);

app.get("/", (_, res) => {
  client.get("counter", (_, counter) => {
    res.send("Page Visitor Counter : " + counter);
    client.set("counter", parseInt(counter) + 1);
  });
});

const PORT = 4001;
app.listen(PORT, () => {
  console.log(`Listening on port '${PORT}'`);
});
